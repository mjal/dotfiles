call plug#begin()
  Plug 'tpope/vim-sensible'
  Plug 'tpope/vim-surround'
  Plug 'tpope/vim-rails'
  Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
  Plug 'dense-analysis/ale'
  Plug 'itchyny/lightline.vim'
  Plug 'takac/vim-hardtime'
  Plug 'dracula/vim', { 'as': 'dracula' }
  Plug 'altercation/vim-colors-solarized'
  Plug 'gilgigilgil/anderson.vim', { 'as': 'anderson' }
  Plug 'kchmck/vim-coffee-script'
  Plug 'digitaltoad/vim-pug'
  Plug 'kien/ctrlp.vim'
  Plug 'tpope/vim-fugitive'
  Plug 'jceb/vim-orgmode'
  Plug 'leafgarland/typescript-vim'
  Plug 'peitalin/vim-jsx-typescript'
  Plug 'elixir-editors/vim-elixir'
  Plug 'godlygeek/tabular'
  Plug 'posva/vim-vue'
call plug#end()

filetype plugin indent on

set mouse=a
set number
set expandtab
set shiftwidth=2
set tabstop=2
set nowrap

syntax enable
colorscheme dracula

let mapleader = " "
noremap <leader>t :NERDTreeToggle<CR>
noremap <leader>ve :vsplit $MYVIMRC<CR>
noremap <leader>vs :source $MYVIMRC<CR>

inoremap jk <esc>
"inoremap kj <esc>l
" inoremap <esc> <nop>

noremap <C-l> <C-w>l
noremap <C-h> <C-w>h
noremap <C-k> <C-w>k
noremap <C-j> <C-w>j

nnoremap <C-e> 2<C-e>
nnoremap <C-y> 2<C-y>

noremap <C-x>3 :vsp<CR>
noremap <C-x>2 :sp<CR>

noremap <leader>q :q<CR>
noremap <leader>wq :wq<CR>
noremap <leader><leader> :w<CR>
noremap <leader>w :w<CR>
noremap <leader>s :w<CR>
noremap <leader>g :G<CR>
noremap <leader>b :ls<CR>:b<Space>
noremap <leader>p :CtrlP<CR>
noremap <leader>a :ALEToggle<CR>
noremap <leader>z zf}
noremap <leader>x za

noremap <leader>, <<
noremap <leader>. >>
" let g:hardtime_default_on = 1

noremap <leader>n :e ~/Documents/notes/notes.org

let g:ctrlp_custom_ignore = 'node_modules\|git'

"\   'vue': ['eslint', 'vls'],
let g:ale_linters = {
\   'ocaml': ['ols'],
\}

"let g:ale_fixers = {
"\   'ocaml': ['ocamlformat'],
"\   '*': ['remove_trailing_lines', 'trim_whitespace'],
"\}
