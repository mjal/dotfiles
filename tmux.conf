# Remap prefix from 'C-b' to 'C-a'
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# Use | and - to split a window vertically and horizontally instead of " and % respoectively
unbind '"'
unbind %
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"

# Mouse
set-option -g mouse on

# Automatically set window title
set-window-option -g automatic-rename on
set-option -g set-titles on

# Start window and pane indices at 1
set -g base-index 1
set -g pane-base-index 1

# Length of tmux status line
set -g status-left-length 30
set -g status-right-length 150

set-option -g status "on"

# Default statusbar color
set-option -g status-style bg=colour237,fg=colour223 # bg=bg1, fg=fg1

# Default window title colors
set-window-option -g window-status-style bg=colour214,fg=colour237 # bg=yellow, fg=bg1

# Default window with an activity alert
set-window-option -g window-status-activity-style bg=colour237,fg=colour248 # bg=bg1, fg=fg3

# Active window title colors
set-window-option -g window-status-current-style bg=red,fg=colour237 # fg=bg1

# Set active pane border color
set-option -g pane-active-border-style fg=colour214

# Set inactive pane border color
set-option -g pane-border-style fg=colour239

# Message info
set-option -g message-style bg=colour239,fg=colour223 # bg=bg2, fg=fg1

# Writing commands inactive
set-option -g message-command-style bg=colour239,fg=colour223 # bg=fg3, fg=bg1

# Pane number display
set-option -g display-panes-active-colour colour1 #fg2
set-option -g display-panes-colour colour237 #bg1

set-option -g status-left "#[fg=colour7, bg=colour241]#{?client_prefix,#[bg=colour167],} ❐ #S #[fg=colour1] ❐ "


# Open ~/.tmux.conf in vim and reload settings on quit
unbind e
bind e new-window -n '~/.tmux.conf' "sh -c 'nvim ~/.tmux.conf && tmux source ~/.tmux.conf'"

# Use m to toggle mouse mode
unbind m
bind m setw mouse

# Use r to quickly reload tmux settings
unbind r
bind r \
	source-file ~/.tmux.conf \;\
	display 'Reloaded tmux config'

# bind P paste-buffer
# bind-key -T copy-mode-vi v send-keys -X begin-selection
# bind-key -T copy-mode-vi y send-keys -X copy-selection
# bind-key -T copy-mode-vi r send-keys -X rectangle-toggle

bind -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "xclip -i -f -selection primary | xclip -i -selection clipboard"
