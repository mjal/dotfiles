return require('packer').startup(function()
  use 'wbthomason/packer.nvim'

  use {'dracula/vim', as = 'dracula'}

  use {
    'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} }
  }

  use {
    'phaazon/hop.nvim',
    branch = 'v2', -- optional but strongly recommended
    config = function()
      -- you can configure Hop the way you like here; see :h hop-config
      require'hop'.setup { keys = 'etovxqpdygfblzhckisuran' }
    end
  }

  use({
    "ms-jpq/chadtree",
    branch = "chad",
    run = ":CHADdeps",
  })

  use({'ms-jpq/coq_nvim', branch = 'coq'})
  -- use({'ms-jpq/coq.artifacts', branch = 'artifacts'})
  use({'ms-jpq/coq.thirdparty', branch = '3p'})

  use 'jreybert/vimagit'

  use { 'TimUntersberger/neogit', requires = 'nvim-lua/plenary.nvim' }
  use 'tpope/vim-fugitive'

  use {
    "folke/which-key.nvim",
    config = function()
      require("which-key").setup {
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      }
    end
  }

end)
