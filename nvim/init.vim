" git gutter
" vim surround
" formatte

lua require("plugins")

" lua require("bindings")

""" --- Config
set mouse=a
set expandtab
set shiftwidth=2
set tabstop=2
set nowrap
" set guifont=Hack:h8

""" --- Theme
colorscheme dracula

""" --- Bindings

let mapleader = " "

" Quick escape
" inoremap jk <esc>

" Saving and quiting
noremap <leader>q :q<CR>
noremap <leader>s :w<CR>
noremap <leader>w :w<CR>
noremap <leader>wq :wq<CR>

" Scrolling
nnoremap <C-e> 2<C-e>
nnoremap <C-y> 2<C-y>

" Splitting
noremap <C-x>3 :vsp<CR>
noremap <C-x>2 :sp<CR>
noremap <leader>3 :vsp<CR>
noremap <leader>2 :sp<CR>

" Navigation
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h
noremap <C-k> <C-w>k
noremap <C-j> <C-w>j

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>. <cmd>Telescope oldfiles<cr>

" Edit vimrc
noremap <leader>ve :vsplit $MYVIMRC<CR>
noremap <leader>vd :vsplit ~/.config/nvim<CR>
noremap <leader>vs :source $MYVIMRC<CR>

" Line number
noremap <leader>n :set number!<CR>
noremap <leader><leader>n :set relativenumber!<CR>

" Hop
noremap <leader>h :HopWord<CR>
noremap <leader><leader>f :HopWordAC<CR>
noremap <leader><leader>F :HopWordBC<CR>

" Chadtree
nnoremap <leader>t <cmd>CHADopen<cr>
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
    \ execute 'CHADopen' | execute 'cd '.argv()[0] | endif

" Coq (auto completion)
nnoremap <leader>c <cmd>COQnow -s<cr>

" Git
" nnoremap <leader>g <cmd>Magit<cr>
nnoremap <leader>g <cmd>Neogit<cr>
